from bs4 import BeautifulSoup
import requests
import subprocess
import os
import ctypes
import platform
import time
import tempfile
import logging
import configparser
import traceback
import shutil
import datetime

try:
    __file__ = __file__
except NameError:
    import sys
    __file__ = sys.argv[0]

logging.basicConfig(level=logging.DEBUG, filename=os.path.join(os.path.abspath(os.path.dirname(__file__)), "log"),
                    filemode="a")


class WallhavenChanger:
    def __init__(self, try_change_count=3, category_general=True, category_anime=True, category_people=True,
                 purity_sfw=True, purity_sketchy=True, purity_nsfw=False, resolutions="", ratios="", sorting="random",
                 order="desc", save_dir=None):
        self.try_change_count = int(try_change_count)
        self.categories = str(int(category_general)) + str(int(category_anime)) + str(int(category_people))
        self.purity = str(int(purity_sfw)) + str(int(purity_sketchy)) + str(int(purity_nsfw))
        self.resolutions = resolutions
        self.ratios = ratios
        self.sorting = sorting
        self.order = order
        if save_dir is not None:
            if platform.system() == "Windows":
                self.save_dir = (save_dir if save_dir[0] != '~' else os.path.expanduser(save_dir)) \
                    if save_dir[0] != '.' \
                    else os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), save_dir))
            elif platform.system() == "Linux":
                self.save_dir = os.path.expanduser(save_dir) if save_dir[0] == '~' \
                    else os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), save_dir)) \
                    if save_dir[0] != '/' else save_dir

            logging.debug("SaveDir: %s", self.save_dir)

            self.save_dir = os.path.abspath(self.save_dir)

            if not os.path.exists(self.save_dir):
                logging.debug("Creating dir: %s", self.save_dir)
                os.makedirs(self.save_dir)
                logging.info("Dir created: %s", self.save_dir)
            else:
                logging.debug("Dir %s exists", self.save_dir)
        else:
            self.save_dir = None

    def change(self):
        try:
            for i in range(0, self.try_change_count):
                image_number = self._get_image_number()
                if image_number is None:
                    continue

                image_path = self._download_image(image_number)
                if image_path is None:
                    continue

                self._change_wallpaper(image_path)

                self._save_image(image_path)

                os.remove(image_path)

                break
        except:
            logging.critical("Some error\n%s", str(traceback.format_exc()))

    def _save_image(self, image_path):
        logging.debug("Saving image from: %s", image_path)
        if self.save_dir is None:
            logging.debug("Save is off")
            return

        save_path = os.path.join(self.save_dir,
                                 "Wallhaven-{0}.jpg".format(datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S')))

        logging.debug("Trying to save to: %s", save_path)

        shutil.copy(image_path, save_path)

    def _change_wallpaper(self, image_path):
        if platform.system() == "Windows":
            SPI_SETDESKWALLPAPER = 20
            ctypes.windll.user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, image_path, 3)
        elif platform.system() == "Linux":
            subprocess.call(["gsettings", "set", "org.gnome.desktop.background", "picture-uri",
                             "file://" + image_path])
            time.sleep(2)

        logging.info("Walpaper changed")

    def _download_image(self, image_number):
        image_data = requests.get(
            'https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-{0}.jpg'.format(str(image_number)), stream=True,
            verify=False)

        logging.debug("Image page loaded with code %d", image_data.status_code)

        if image_data.status_code != 200:
            return None

        status = False

        with tempfile.NamedTemporaryFile(delete=False) as image_file:
            logging.debug("Temp image file path: %s", image_file.name)

            for chunk in image_data.iter_content(1024):
                image_file.write(chunk)

            logging.debug("Image file size is %d", os.stat(image_file.name).st_size)

            if os.stat(image_file.name).st_size > 1024:
                status = True

        if not status:
            os.remove(image_file.name)

        return None if not status else image_file.name

    def _get_image_number(self):
        logging.debug("Trying obtain image number")

        random_page_data = requests.get('https://alpha.wallhaven.cc/search', params={"categories": self.categories,
                                                                                     "purity": self.purity,
                                                                                     "resolutions": self.resolutions,
                                                                                     "ratios": self.ratios,
                                                                                     "sorting": self.sorting,
                                                                                     "order": self.order},
                                        verify=False)

        logging.debug("Random page request code %d", random_page_data.status_code)

        if random_page_data.status_code != 200:
            return None

        logging.debug("Random page loaded")

        figure_tag = BeautifulSoup(random_page_data.text, "html.parser")\
            .select("#thumbs > section:nth-of-type(1) > ul > li:nth-of-type(1) > figure[data-wallpaper-id]")

        logging.debug("Found %d image tags", len(figure_tag))

        if not len(figure_tag):
            return None

        image_number = figure_tag[0].attrs['data-wallpaper-id']

        logging.debug("Got image number %s", image_number)

        return image_number


def main():
    while True:
        config = configparser.ConfigParser()
        config_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "config")

        if os.path.exists(config_path):
            config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), "config"), encoding="utf-8")
        else:
            if not os.path.exists(os.path.dirname(config_path)):
                os.makedirs(os.path.dirname(config_path))

            config["DEFAULT"] = {"TryChangeCount": "3", "Resolutions": "", "Ratios": "", "Sorting": "random",
                                 "Order": "desc", "SaveDir": "", "ChangeTimeout": "300"}
            config["CATEGORIES"] = {"General": "1", "Anime": "1", "People": "1"}
            config["PURITY"] = {"SFW": "1", "Sketchy": "1", "NSFW": "0"}
            with open(config_path, 'w', encoding="utf-8") as configfile:
                config.write(configfile)

        try:
            wallhaven_changer = WallhavenChanger(try_change_count=config["DEFAULT"]["TryChangeCount"],
                                                 category_general=config["CATEGORIES"]["General"],
                                                 category_anime=config["CATEGORIES"]["Anime"],
                                                 category_people=config["CATEGORIES"]["People"],
                                                 purity_sfw=config["PURITY"]["SFW"],
                                                 purity_sketchy=config["PURITY"]["Sketchy"],
                                                 purity_nsfw=config["PURITY"]["NSFW"],
                                                 resolutions=config["DEFAULT"]["Resolutions"],
                                                 ratios=config["DEFAULT"]["Ratios"],
                                                 sorting=config["DEFAULT"]["Sorting"],
                                                 order=config["DEFAULT"]["Order"],
                                                 save_dir=config["DEFAULT"]["SaveDir"] if len(config["DEFAULT"]["SaveDir"])
                                                 else None)
        except:
            logging.critical("Config file error\n%s", str(traceback.format_exc()))
            return

        wallhaven_changer.change()

        if len(config["DEFAULT"]["ChangeTimeout"]) and config["DEFAULT"]["ChangeTimeout"] != "0":
            time.sleep(int(config["DEFAULT"]["ChangeTimeout"]))
        else:
            return

if __name__ == "__main__":
    main()