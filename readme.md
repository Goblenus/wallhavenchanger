# WallhavenChanger
---
This script helps you to change your wallpaper without going to [wallhaven](https://alpha.wallhaven.cc/).

# Instalation
---
### Windows: ###
- Download WallhavenChanger.py and config files
- Install [Python 3](https://www.python.org/downloads/windows/)
- From pip install bs4 (pip install bs4)
- From pip install requests (pip install requests)

### Linux: ###
- Download WallhavenChanger.py and config files
- sudo apt-get install python3
- sudo apt-get install pip3
- sudo pip3 install bs4
- sudo pip3 install requests
 # Config Settings
---
## DEFAULT section
- TryChangeCount, parameter that sets how many times script trying to obtain image from wallhaven
- Resolutions, image resolutions
- Ratios, image ratio
- Sorting, finding method
- Order, image order
## CATEGORIES section
- General, images from general category
- Anime, images from general anime
- People, images from general people
## PURITY section
- SFW, images with sfw purity
- Sketchy, images with sketchy purity
- NSFW, images with nsfw purity (need registration, not implemented yet)